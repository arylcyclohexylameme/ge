defmodule Ge do
  @moduledoc false

  use Application

  def start(_type, _args) do
    Ge.Supervisor.start_link

    for n <- 15675..60000 do
      Ge.Server.add_item(n)
      IO.inspect(Ge.Server.get_item(n))
      if(rem(n, 5) == 0, do: :timer.sleep(5000))
    end

  end

end
