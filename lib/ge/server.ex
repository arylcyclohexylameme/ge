defmodule Ge.Server do
  @moduledoc false

  use GenServer

  def req_item(item) do
    url = "http://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json"
    res = HTTPoison.get(
      url,
      [],
      params: %{
        item: item
      }
    )
    case res do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        try do
          {:ok, Poison.decode!(body)["item"]}
        rescue
          e in Poison.SyntaxError -> {:error, e}
        end
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        {:error, :not_found}
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end

  def match(map, key, value) do
    if is_map(map) && Map.has_key?(map, key) do
      value == map[key]
    else
      false
    end
  end

  def first_match(collection, key, value) do
    res = Enum.find(collection, fn (element) -> match(element, key, value) end)
    case res do
      nil -> {:not_found, %{key => value}}
      _ -> {:found, res}
    end
  end

  def remove(collection, key, value) do
    {found, res} = first_match(collection, key, value)
    if found == :not_found do
      collection
    else
      List.delete(collection, res)
    end
  end

  def start_link do
    GenServer.start_link(__MODULE__, [], name: :ge)
  end

  def init(_opts) do
    {:ok, []}
  end

  def handle_call({:get, item}, _from, state) do
    {:reply, first_match(state, "id", item), state}
  end

  def handle_call(:all, _from, state) do
    {:reply, state, state}
  end

  def handle_call(_msg, _from, state) do
    {:reply, :ok, state}
  end

  def handle_cast({:add, item}, state) do
    {ok, res} = req_item(item)
    if ok == :ok do
      Mongo.replace_one(:mongo, "ge", %{"id" => item}, res, upsert: :true)
      {:noreply, [res | remove(state, "id", item)]}
    else
      {:noreply, state}
    end
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def add_item(id) do
    GenServer.cast(:ge, {:add, id})
  end

  def get_item(id) do
    GenServer.call(:ge, {:get, id})
  end

  def all_items do
    GenServer.call(:ge, :all)
  end
end