defmodule Ge.Supervisor do
  @moduledoc false

  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def init(_) do
    children = [
#    Mongo.start_link(database: "ge")
      worker(Ge.Server, [], restart: :temporary),
      worker(Mongo, [[name: :mongo, database: "ge"]])
    ]

    opts = [strategy: :one_for_one, name: Ge.Supervisor]
    supervise(children, opts)
  end
end
